const fs = require('fs');
const pdf = require('html-pdf');
const hbs = require('handlebars');
const path = require('path');

let options = {
    format: 'Letter',
    width: '800px',
    height: '700px',
    base: 'file://' + __dirname + '/',
    quality: "100"
};

/**
 * Main convert function
 *
 * @returns {Promise}
 */
let convert = function() {
    return new Promise((res, rej) => {

        let data = JSON.parse(fs.readFileSync(path.join(__dirname, 'input.json'), 'utf-8').toString());
        let layout = getTpl('layout');

        let content = '';

        for (let page of data) {
            let template = getTpl(page.template);
            if (template) {
                content += template(page.content);
            }
        }


        let html = layout({ content });

        pdf.create(html, options).toFile('./output.pdf', function (err) {
            if (err) {
                rej(err);
                console.log(err);
            } else {
                res();
            }
        });
    });
};

/**
 * Load template helper
 *
 * @param name
 * @returns {string|null}
 */
function getTpl(name) {
    try {
        return hbs.compile(fs.readFileSync(path.join(__dirname, 'templates', name + '.hbs'), 'utf-8').toString());
    } catch (e) {
        return null;
    }
}

convert().then(() => console.log('done'));

module.exports = convert;